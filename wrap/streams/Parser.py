#-*- coding: utf-8 -*-

from reactor.shortcuts import *

import pymongo

#*******************************************************************************

from pattern.en import parsetree
from pattern.search import search, Pattern, taxonomy
from pattern.metrics import similarity, levenshtein, readability, cooccurrence
from pattern.vector import stem, PORTER, LEMMA, chngrams

import nltk, textblob
import quepy

################################################################################

@Reactor.wamp.register_middleware('think.linguistic.Wernicke')
class Wernicke(Reactor.wamp.Nucleon):
    def on_open(self, details):
        #nltk.download('book')

        for f in ('rose', 'lily', 'daisy', 'daffodil', 'begonia'):
            taxonomy.append(f, type='flower')

    #***************************************************************************

    def on_close(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'brain.vocabulary.sentence')
    def on_sentence(self, stce):
       print("event received on {}: {}".format(topic, msg))

    #***************************************************************************

    @Reactor.wamp.register_topic(u'brain.vocabulary.word')
    def on_new_word(self, msg):
       print("event received on {}: {}".format(topic, msg))

    ############################################################################

    @Reactor.wamp.register_method(u'brain.grammar.tokenize')
    def nltk_tokenize(self, lang_id, statement):
        resp = {
            'language': lang_id,
            'document': statement,
            'analyzed': [],
        }

        for stce in nltk.sent_tokenize(resp['document']):
            item = {
                'sentence': stce,
            }

            item['words']   = nltk.word_tokenize(item['sentence'])
            item['pos_tag'] = nltk.pos_tag(item['words'])

            resp['analyzed'].append(item)

        return resp

    #***************************************************************************

    @Reactor.wamp.register_method(u'brain.vocabulary.words')
    def pattern_words(self, lang_id, *literals):
        resp = {
            'language': lang_id,
            'literals': literals,
            'analyzed': [],
        }

        for literal in literals:
            resp['analyzed'].append(self.wrap_pattern_word(literal, semantic=True, ontology=True, chngram=(2,3,4,5,6)))

        return resp

    ############################################################################

    # invoke('grammar.search',  "tasty cat food",   'DT? RB? JJ? NN+')
    # invoke('grammar.search',  "big white rabbit", 'NN','NP')
    # invoke('grammar.match',   "Chuck Norris is cooler than Dolph Lundgren.", 'NP be ADJP|ADVP than NP')
    # invoke('grammar.pattern', "Chuck Norris is cooler than Dolph Lundgren.", '{NP} be * than {NP}')
    # invoke('grammar.cooccur', "Chuck Norris is cooler than Dolph Lundgren.", 'NN', 'NN')

    # invoke('vocab.assess',    "Chuck Norris is cooler than Dolph Lundgren.")

    #***************************************************************************

    def wrap_pattern_word(self, word, related=None, semantic=False, ontology=False, chngram=None):
        resp = {
            'text': unicode(word),
            'stem': stem(unicode(word), stemmer=LEMMA),
        }

        if related is not None:
            resp['role'] = related.constraint(word)

        if semantic:
            resp.update({
                'porter': stem(word, stemmer=PORTER),
                'lemma':  stem(word, stemmer=LEMMA),
            })

        if ontology:
            pass

        if type(chngram) in (int, long, float):
            chngram = [chngram]

        if type(chngram) in (tuple, set, frozenset, list):
            resp['gram'] = dict([
                (size,chngrams('The cat sat on the mat.'.lower(), n=size))
                for size in chngram
            ])

        self.publish(u'brain.vocabulary.word')

        return resp

    ############################################################################

    @Reactor.wamp.register_method(u'brain.vocabulary.assess')
    def expr_assess(self, literal):
        return dict(
            readable = readability(literal)
        )

    #***************************************************************************

    @Reactor.wamp.register_method(u'brain.vocabulary.compare')
    def expr_compare(self, x, y):
        return dict(
            similar = similarity(x, y),
            levensh = levenshtein(x, y),
        )

    #***************************************************************************

    @Reactor.wamp.register_method(u'brain.grammar.cooccur')
    def expr_occure(self, literal, x, y):
        m = cooccurrence(literal, # open('pattern/test/corpora/tagged-en-oanc.txt')
               window = (-2, -1),
                term1 = lambda w: w[1] == x,
                term2 = lambda w: w[1] == y,
            normalize = lambda w: tuple(w.split('/')) # cat/NN => ('cat', 'NN') 
        )

        resp = []

        for noun in m:
            for adjc, count in m[noun].items():
                nrw = {
                    noun[1]: noun[0],
                    adjc[1]: adjc[0],
                }

                resp[nrw] = count

        return resp

    ############################################################################

    @Reactor.wamp.register_method(u'brain.grammar.search')
    def expr_search(self, value, *filters):
        stce = parsetree(value, lemmata=True)

        resp = []

        for expr in filters:
            for word in search(expr, stce).words:
                literal = unicode(word)

                if literal not in resp:
                    resp.append(literal)

        return resp

    #***************************************************************************

    @Reactor.wamp.register_method(u'brain.grammar.match')
    def expr_match(self, expr, value):
        stce = parsetree(value, lemmata=True)

        resp = []

        mach = match(expr, stce)

        for word in mach:
            resp.append(dict(
                word = unicode(word),
                role = mach.constraint(word),
            ))

        return resp

    #***************************************************************************

    @Reactor.wamp.register_method(u'brain.grammar.pattern')
    def expr_pattern(self, expr):
        stce = parsetree(value, lemmata=True)

        resp = []

        mach = Pattern.fromstring().match(stce)

        for group in range(0, len(mach)):
            for word in mach.group(group + 1):
                resp.append(self.wrap_pattern_word(word))

        return resp

    ############################################################################

    @Reactor.wamp.register_method(u'brain.vocabulary.sparql')
    def vocab_sparql(self, literal, roles=['NN']):
        qs = [
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>',
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>',
            'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
            'PREFIX skos: <http://www.w3.org/2004/02/skos/core#>',
            'PREFIX quepy: <http://www.machinalis.com/quepy#>',
            'PREFIX dbpedia: <http://dbpedia.org/ontology/>',
            'PREFIX dbpprop: <http://dbpedia.org/property/>',
            'PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>',
            'PREFIX type: <http://dbpedia.org/class/yago/>',
            'PREFIX prop: <http://dbpedia.org/property/>',
            '',
        ]

        qs += ['SELECT ?word, ?link, ?type WHERE {']

        qs += ['    ?link rdf:type   ?type.']
        qs += ['    ?link rdfs:label "%s"@en.']
        qs += ['    ?link rdfs:label ?word.']

        qs += ['}']

        qs = '\n'.join(qs)

        return self.invoke('data.rdf.sparql', ['dbpedia'], qs)

################################################################################

@Reactor.wamp.register_middleware('think.linguistic.Broca')
class Broca(Reactor.wamp.Nucleon):
    def on_open(self, details):
        self.que = quepy.install("reactor.dialect.generic")
        #self.markov = quepy.install("reactor.dialect.generic")

    ############################################################################

    @Reactor.wamp.register_topic(u'brain.dialect.statement')
    def on_statement(self, msg):
        print("event received on {}: {}".format(topic, msg))

    #***************************************************************************

    @Reactor.wamp.register_topic(u'brain.dialect.question')
    def on_question(self, msg):
        print("event received on {}: {}".format(topic, msg))

    #***************************************************************************

    @Reactor.wamp.register_topic(u'brain.dialect.order')
    def on_order(self, msg):
        print("event received on {}: {}".format(topic, msg))

    ############################################################################

    @Reactor.wamp.register_method(u'brain.dialect.assess')
    def process(self, stmt):
        pass

    #***************************************************************************

    @Reactor.wamp.register_method(u'brain.dialect.answer')
    def answer(self, stmt):
        data = {
            'statement': stmt,
        }

        context = {
            'grammars': [
                dict(name='minimal', corpus='brown', rules=[
                    'NN: {<NN><NN>}',
                    'NP: {<DT>?<JJ>*<NN>}',
                    'CHUNK: {<V.*> <TO> <V.*>}',
                ]),
                dict(name='default', corpus='brown', rules=[
                    'NP:'
                    '{<.*>+}          # Chunk everything'
                    '}<VBD|IN>+{      # Chink sequences of VBD and IN'
                ]),
            ],
        }

        for key in ('understand','interpret','express'):
            hnd = getattr(self, 'que_%s' % key, None)

            if callable(hnd):
                context = hnd(context, data)

        return context

    #***************************************************************************

    @Reactor.wamp.register_method(u'brain.dialect.smalltalk')
    def smalltalk(self, stmt):
        pass

    ############################################################################

    def que_understand(self, context, data):
        results = {}

        results['target'], results['query'], results['meta'] = self.que.get_query(data['statement'])

        results['query'] = results['query'].replace('\n', '\n\n')

        while results['query'].startswith('\n'):
            results['query'] = results['query'][1:]

        results['cols'], results['rows'] = Reactor.sparql.table('http://dbpedia.org/sparql', results['query'])

        context['results'] = results

        return context

    #***************************************************************************

    def que_interpret(self, request, context, data):
        context['results'] = Reactor.nlp.parse('en', data['statement'])

        context['results']['entities'] = [
            (entity,role)
            for entry       in context['results']['analyzed']
            for entity,role in entry['pos_tag']
            if role in (
                'NN','NNP','NNS','NNPS',
                'VB','VBD','VBN',
            )
        ]
        context['results']['topics'] = context['results']['entities']

        context['results']['matches'] = []

        for grammar in context['grammars']:
            grammar['book'] = '\n'.join(grammar['rules'])

            match = dict(
                alias   = grammar['name'],
                grammar = grammar['book'],
                corpus  = getattr(nltk.corpus, grammar['corpus'], nltk.corpus.brown),
                parser  = nltk.RegexpParser(grammar['book']),
            )

            match['result']   = [
                self.flatten(item)
                for item in match['parser'].parse(context['results']['entities']).pos()
            ]
            match['recurse']  = match['parser'].parse(context['results']['topics']).pos()

            context['results']['matches'].append(match)

            context['results']['topics'] = match['recurse']

        context['results']['topics']   = [
            self.flatten(item)
            for item in context['results']['topics']
        ]

        return context

    #***************************************************************************

    def que_express(self, request, context, data):
        context['results'] = {}

        datatype,mimetype = 'rdf','application/rdf+xml'
        datatype,mimetype = 'turtle','text/turtle'

        from rdflib.namespace import XMLNS,RDF,RDFS,XSD,OWL,SKOS,DOAP,FOAF,DC,DCTERMS,VOID

        raw = Reactor.sparql.mime('http://dbpedia.org/sparql', mimetype, data['statement'])

        grp = rdflib.Graph()
        grp.parse(format=datatype, data=raw)

        for row in grp.query(Reactor.rdf.NS+"""SELECT DISTINCT ?person ?name WHERE {
    ?person foaf:name ?name .
}"""):
            context['results']['people'].append(row)

        for row in grp.query(Reactor.rdf.NS+"""SELECT DISTINCT ?org ?title WHERE {
    ?org foaf:name ?title .
}"""):
            context['results']['organizations'].append(row)

        return context

    ############################################################################

    def flatten(self, item):
        lst,obj = [], item

        while len(obj)==2:
            obj, val = obj[0], obj[1]

            lst += [val]

        return obj, lst

    #***************************************************************************

    def sparql(self, x):
        pass

