#-*- coding: utf-8 -*-

from reactor.shortcuts import *

from .abstract import *

#*******************************************************************************

import quepy
from SPARQLWrapper import SPARQLWrapper, JSON

################################################################################

class Que(Nucleon):
    def on_open(self, details):
        self.que = quepy.install("reactor.dialect.dbpedia")
        self.markov = quepy.install("reactor.dialect.dbpedia")

    ############################################################################

    def topics(self, details):
        yield u'broca.new_word', self.on_new_word

    #***************************************************************************

    def methods(self, details):
        yield u'intellect.dialect.answer',    self.answer
        yield u'intellect.dialect.smalltalk', self.smalltalk

    ############################################################################

    def on_new_word(self, msg):
        print("event received on {}: {}".format(topic, msg))

    ############################################################################

    def smalltalk(self, stmt):
        pass

################################################################################

[
    "What is a car?",
    "Who is Tom Cruise?",
    "Who is George Lucas?",
    "Who is Mirtha Legrand?",
    # "List Microsoft software",
    "Name Fiat cars",
    "time in argentina",
    "what time is it in Chile?",
    "List movies directed by Martin Scorsese",
    "How long is Pulp Fiction",
    "which movies did Mel Gibson starred?",
    "When was Gladiator released?",
    "who directed Pocahontas?",
    "actors of Fight Club",
]

#*******************************************************************************

class Wonder(BaseIntent):
    cursor = property(lambda self: self.provider.cursor)

################################################################################

class SmallTalk(Wonder):
    def detect(self):
        return 

    def process(self):
        pass

#*******************************************************************************

class Order(BaseIntent):
    def detect(self):
        return False

    def process(self):
        pass

#*******************************************************************************

class Suggestion(Wonder):
    def detect(self):
        return False

    def process(self):
        pass

#*******************************************************************************

class Witness(Wonder):
    def detect(self):
        return True

    def process(self):
        pass

################################################################################

class Message(object):
    def __init__(self, prvd, raw):
        self._prvd = prvd
        self._raw  = raw
        self._prs  = None

    raw = property(lambda self: self._raw)

    @property
    def parsed(self):
        if self._prs is None:
            from textblob import TextBlob, Blobber
            from textblob.np_extractors import FastNPExtractor, ConllExtractor
            from textblob.sentiments import PatternAnalyzer, NaiveBayesAnalyzer
            from textblob.taggers import PatternTagger, NLTKTagger

            tb = Blobber(
                analyzer     = NaiveBayesAnalyzer(),
                pos_tagger   = NLTKTagger(),
                np_extractor = ConllExtractor(),
            )

            self._prs = TextBlob(self.original, analyzer=NaiveBayesAnalyzer())

        return self._prs

    original  = property(lambda self: self.raw['text'])
    processed = property(lambda self: self.parsed.correct())

    words     = property(lambda self: self.parsed.words.singularize())
    sentences = property(lambda self: self.parsed.sentences)

    language  = property(lambda self: self.parsed.detect_language())

    def translate(self, lang): return self.parsed.translate(to=lang)

    sentiment = property(lambda self: self.parsed.sentiment)

#@register('sparql')
class Provider(BaseProvider):
    cursor = property(lambda self: self._cnx)

    def initialize(self):
        self._cnx = SPARQLWrapper("http://dbpedia.org/sparql")

        #self.ns('dbpedia',  auto=True)
        #self.ns('freebase', auto=True)

    def wikipedia2dbpedia(self, link):
        """
        Given a wikipedia URL returns the dbpedia resource
        of that page.
        """

        query = """
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        SELECT * WHERE {
            ?url foaf:isPrimaryTopicOf <%s>.
        }
        """ % link

        self.cursor.setQuery(query)
        self.cursor.setReturnFormat(JSON)

        results = self.cursor.query().convert()

        if not results["results"]["bindings"]:
            print "Snorql URL not found"
            sys.exit(1)
        else:
            return results["results"]["bindings"][0]["url"]["value"]

    #***************************************************************************

    def process_message(self, raw):
        resp = None

        print "Got message '%(text)s' :" % raw

        for handler in (SmallTalk, Question, Witness):
            print "\t-> Checking '%s' ..." % handler

            acte = handler(self, raw)

            if acte.detect():
                for react in acte.process():
                    if hasattr(react, 'render'):
                        react.render()

                        return

################################################################################

class Question(Wonder):
    def initialize(self):
        ns = self.ns('dbpedia', auto=True)

        self.target, self.query, self.meta = ns.get_query(self.text)

        if isinstance(self.meta, tuple):
            self.type = self.meta[0]
            self.meta = self.meta[1]
        else:
            self.type = self.meta
            self.meta = None

    def detect(self):
        return (self.query is not None)

    def process(self):
        if self.target.startswith("?"):
            self.target = self.target[1:]

        if self.query:
            self.cursor.setQuery(self.query)

            self.cursor.setReturnFormat(JSON)

            resp = self.cursor.query().convert()

            if not resp["results"]["bindings"]:
                self.channel.send_message("I don't know :(")

                #raise Exception()

            yield Answer(self, resp)

#*******************************************************************************

class Answer(BaseResults):
    def print_define(self, target, metadata=None):
        for result in self["results"]["bindings"]:
            if result[target]["xml:lang"] == "en":
                yield result[target]["value"]

    def print_enum(self, target, metadata=None):
        used_labels = []

        for result in self["results"]["bindings"]:
            if result[target]["type"] == u"literal":
                if result[target]["xml:lang"] == "en":
                    label = result[target]["value"]
                    if label not in used_labels:
                        used_labels.append(label)

                        yield label

    def print_literal(self, target, metadata=None):
        for result in self["results"]["bindings"]:
            literal = result[target]["value"]
            if metadata:
                yield metadata.format(literal)
            else:
                yield literal

    def print_time(self, target, metadata=None):
        gmt = time.mktime(time.gmtime())
        gmt = datetime.datetime.fromtimestamp(gmt)

        for result in self["results"]["bindings"]:
            offset = result[target]["value"].replace(u"−", u"-")

            if ("to" in offset) or ("and" in offset):
                if "to" in offset:
                    connector = "and"
                    from_offset, to_offset = offset.split("to")
                else:
                    connector = "or"
                    from_offset, to_offset = offset.split("and")

                from_offset, to_offset = int(from_offset), int(to_offset)

                if from_offset > to_offset:
                    from_offset, to_offset = to_offset, from_offset

                from_delta = datetime.timedelta(hours=from_offset)
                to_delta = datetime.timedelta(hours=to_offset)

                from_time = gmt + from_delta
                to_time = gmt + to_delta

                location_string = random.choice(["where you are",
                                                 "your location"])

                yield "Between %s %s %s, depending on %s" % \
                      (from_time.strftime("%H:%M"),
                       connector,
                       to_time.strftime("%H:%M on %A"),
                       location_string)

            else:
                offset = int(offset)

                delta = datetime.timedelta(hours=offset)
                the_time = gmt + delta

                yield the_time.strftime("%H:%M on %A")

    def print_age(self, target, metadata=None):
        assert len(self["results"]["bindings"]) == 1

        birth_date = self["results"]["bindings"][0][target]["value"]
        year, month, days = birth_date.split("-")

        birth_date = datetime.date(int(year), int(month), int(days))

        now = datetime.datetime.utcnow()
        now = now.date()

        age = now - birth_date

        yield "{} years old".format(age.days / 365)

    def render(self):
        handler = {
            "define":  self.print_define,
            "enum":    self.print_enum,
            "time":    self.print_time,
            "literal": self.print_literal,
            "age":     self.print_age,
        }[self.wonder.type]

        if callable(handler):
            for msg in handler(self.wonder.target, self.wonder.meta):
                if type(msg) in (str, unicode):
                    if len(msg):
                        self.channel.send_message(msg)

