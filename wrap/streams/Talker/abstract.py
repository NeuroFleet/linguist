#-*- coding: utf-8 -*-

from reactor.shortcuts import *

from pattern import en      as LNG
from pattern import vector  as VEX
from pattern import metrics as MTR

################################################################################

class Particle(str):
    pass

################################################################################

class Word(Particle):
    singular   = property(lambda self: Word(LANG.singularize(str(self))))
    plural     = property(lambda self: Word(LANG.pluralize(str(self))))

    referenced = property(lambda self: Phrase(LANG.referenced(str(self), article=LANG.DEFINITE)))
    definite   = property(lambda self: Phrase(LANG.referenced(str(self), article=LANG.INDEFINITE)))

    suggests   = property(lambda self: [Word(x) for x in LANG.suggest(str(self))])

    synsets    = property(lambda self: wordnet.synsets(str(self))[0])

    def levenstein(self, x): return MTR.similarity(str(self), x, metric=MT.LEVENSHTEIN)
    def similarity(self, x): return MTR.similarity(str(self), x, metric=MT.DICE)

#*******************************************************************************

class Adjective(Particle):
    comparative = property(lambda self: Adjective(LANG.comparative(str(self))))
    superlative = property(lambda self: Adjective(LANG.superlative(str(self))))

#*******************************************************************************

class Verb(Particle):
    lemma       = property(lambda self: LANG.lemma(str(self)))
    lexeme      = property(lambda self: [Verb(x) for x in LANG.lexeme(str(self))])
    tenses      = property(lambda self: LANG.tenses(str(self)))

    #    tense = PRESENT,        # INFINITIVE, PRESENT, PAST, FUTURE
    #   person = 3,              # 1, 2, 3 or None
    #   number = SINGULAR,       # SG, PL
    #     mood = INDICATIVE,     # INDICATIVE, IMPERATIVE, CONDITIONAL, SUBJUNCTIVE
    #   aspect = IMPERFECTIVE,   # IMPERFECTIVE, PERFECTIVE, PROGRESSIVE 
    #  negated = False,          # True or False
    #    parse = True
    def conjugate(self, *args, **kwargs):
        return LANG.conjugate(str(self), *args, **kwargs)

################################################################################

class Phrase(Particle):
    def ngrams(self, level=2):
        return [
            tuple([
                Word(x)
                for x in l
            ])
            for l in LANG.ngrams(str(self), n=level)
        ]

    words       = property(lambda self: Vec.count(Vec.words(str(self)), stemmer=Vec.LEMMA))

    readability = property(lambda self: MTR.readability(str(self)))

    tags    = property(lambda self: [
        (word,pos)
        for word,pos in LANG.tag(str(self))
    ])

    nouns   = property(lambda self: [
        (word,pos)
        for word,pos in LANG.tag(str(self))
        if pos in ('NP', 'NN')
    ])
    verbs   = property(lambda self: [
        (word,pos)
        for word,pos in LANG.tag(str(self))
        if pos in ('V', 'VP')
    ])

    adjs   = property(lambda self: [
        (word,pos)
        for word,pos in LANG.tag(str(self))
        if pos in ('JJ', 'ADV')
    ])

    @property
    def sentences(self):
        return LANG.parsetree(str(self), relations=True, lemmata=True)

    chunks  = property(lambda self: [
        c
        for c in s.chunks
        for s in self.sentences
    ])

    mood  = property(lambda self: [
        (
            s,
            LANG.mood(s),
            LANG.modality(s),
        )
        for s in self.sentences
    ])

