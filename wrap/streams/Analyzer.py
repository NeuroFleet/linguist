#-*- coding: utf-8 -*-

from reactor.shortcuts import *

import pymongo

#*******************************************************************************

from pattern.en import parsetree
from pattern.search import search, Pattern, taxonomy
from pattern.metrics import similarity, levenshtein, readability, cooccurrence
from pattern.vector import stem, LEMMA, Document, Model, TFIDF, HIERARCHICAL

import nltk, textblob
import quepy

################################################################################

@Reactor.wamp.register_middleware('media.Text.Blob')
class Blob_Processor(Reactor.wamp.Nucleon):
    def on_open(self, details):
        pass # nltk.download('book')

    #***************************************************************************

    def on_close(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'media.text.sentence')
    def on_sentence(self, stce):
       print("event received on {}: {}".format(topic, msg))

    ############################################################################

    @Reactor.wamp.register_method(u'media.text.assess')
    def textblob_assess(self, source):
        blob = textblob.TextBlob(source)

        return {
            'document': source,
            'tags':     [tag  for tag  in blob.tags],
            'nouns':    [stce for stce in blob.noun_phrases],
        }

        for stce in blob.sentences:
            item = {
                'phrase': unicode(stce),
            }

            item['polarity'] = sentence.sentiment.polarity

            resp['phrases'].append(item)

        return resp

    #***************************************************************************

    @Reactor.wamp.register_method(u'media.text.phrases')
    def textblob_phrases(self, source):
        blob = textblob.TextBlob(source)

        resp = []

        for stce in blob.sentences:
            item = {
                'phrase': unicode(stce),
            }

            item['polarity'] = sentence.sentiment.polarity

            resp.append(item)

        return resp

    #***************************************************************************

    @Reactor.wamp.register_method(u'media.text.translate')
    def textblob_translate(self, source, language='en'):
        blob = textblob.TextBlob(source)

        resp = blob.translate(to=language)

        return resp

    ############################################################################

    @Reactor.wamp.register_method(u'media.text.keywords')
    def vector_keywords(self, source, threshold=1, limit=6):
        doc = Document(source, threshold=threshold)

        return doc.keywords(top=limit)

    #***************************************************************************

    @Reactor.wamp.register_method(u'media.text.classify')
    def vector_classify(self, sources, **options):
        context = {
            'wrap':  {},
            'docs':  {},
            'score': {},
        }

        index = 0

        for value,config in sources:
            context['docs'][index] = dict(
                index  = index,
                value  = value,
                config = config,
                #threshold = threshold,
            )

            context['wrap'][index] = Document(value, **config)

            context['docs'][index].update({
                'keyword': context['wrap'][index].keywords(top=6),
                'vector': context['wrap'][index].vector,
            })

            index += 1

        context['model'] = Model(documents=[d for d in context['wrap'].values()], weight=TFIDF)

        for x in context['wrap'].keys():
            for y in context['wrap'].keys():
                if x!=y:
                    nrw = '%s-%s' % (x,y)

                    if nrw not in context['score']:
                        context['score'][nrw] = context['model'].similarity(context['wrap'][x],context['wrap'][y])

        if False:
            m.reduce(2)

            for d in m.documents:
                print d.name

                for concept, w1 in m.lsa.vectors[d.id].items():
                    for feature, w2 in m.lsa.concepts[concept].items():
                        if w1 != 0 and w2 != 0:
                            print (feature, w1 * w2)

        if False:
            context['cluster'] = context['model'].cluster(method=HIERARCHICAL, k=2)

        del context['wrap'], context['model']

        return context

