from nucleon.linguist.shortcuts import *

#*******************************************************************************

@render_to('linguist/home.html')
def homepage(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return redirect('done')

#*******************************************************************************

@login_required
@render_to('linguist/parser.html')
def do_parser(request):
    resp = {
        'stats': [
    dict(color='red',    icon='math',  label="Formulas",     value=0, total=150),
    dict(color='green',  icon='book',  label="Vocabularies", value=0, total=50),
    dict(color='purple', icon='speak', label="Dialects",     value=0, total=100),
    dict(color='blue',   icon='link',  label="References",   value=0, total=20),
        ],
        'frm_semantic': CrawlerForm()
    }

    if request.method=='POST':
        resp['frm_semantic'] = SemanticForm(request.POST)

    return resp

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    #url(r'^search/(?P<facet>[^/]+)$',       Search.perform,    name='search'),

    url(r'^parser$',                        do_parser),
    url(r'^$',                              homepage),
]

